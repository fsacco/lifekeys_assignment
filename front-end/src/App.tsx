import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { orderBy } from "lodash";
import {
  RestContext  
} from "./getAllAppointments";

const App: React.FC = () => {
  return (
      <>
        <Route
          path={`/appointments`}
          exact={true}
          component={({ match }: any) => <AllAppointments />}
        />
        <Route
          path={`/appointments/filter/:name`}
          component={({ match }: any) => (
            <FilteredAppointments name={match.params.name} />
          )}
        />
      </>
  );
};

export default App;

function AppointmentComponent({
  appointment: { name, timestamp }
}: {
  appointment: Appointment;
}) {
  return (
    <li>
      {name},{" "}
      {new Date(timestamp).toLocaleString("it-IT", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit"
      })}
    </li>
  );
}

const AppointmentComponentMemo = React.memo(AppointmentComponent);

function AppointmentListComponent({
  appointments
}: {
  appointments: Array<Appointment>;
}) {
  return (
    <ul>
      {/* using index as key because missing unique identifier on appointment */}
      {appointments.map((appointment, index) => (
        <AppointmentComponentMemo key={index} appointment={appointment} />
      ))}
    </ul>
  );
}

// Da rivedere in caso:
//
// function AllAppointments() {
//   let appointments: Array<Appointment> = [];
//   const orderedAppointments = orderBy(
//     appointments,
//     a => a.timestamp
//   );
//   getAllAppointments().then(a => appointments = a)
//   return <AppointmentListComponent appointments={orderedAppointments}/>
// }

export function AllAppointments() {
  const [appointments, setAppointments] = React.useState([] as Array<
    Appointment
  >);
  const orderedAppointments = React.useMemo(
    () => orderBy(appointments, appointment => appointment.timestamp),
    [appointments]
  ); // array dei valori da cui deriva (che cambiano)
  const { getAllAppointments } = React.useContext(RestContext);
  React.useEffect(() => {
    getAllAppointments().then(a => setAppointments(a));
  }, [getAllAppointments]);
  return <AppointmentListComponent appointments={orderedAppointments} />;
}

export function FilteredAppointments({ name }: { name: string }) {
  const [appointments, setAppointments] = React.useState([] as Array<
    Appointment
  >);
  const orderedAppointments = React.useMemo(
    () => orderBy(appointments, appointment => appointment.timestamp),
    [appointments]
  ); // array dei valori da cui deriva (che cambiano)
  const { getAppointmentsByName } = React.useContext(RestContext);
  React.useEffect(() => {
    getAppointmentsByName(name).then(a => setAppointments(a));
  }, [name]);
  return <AppointmentListComponent appointments={orderedAppointments} />;
}

export type Appointment = {
  name: string;
  timestamp: number;
};
