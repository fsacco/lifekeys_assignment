import React from "react";
import Enzyme, { shallow, mount } from "enzyme";
import App, { AllAppointments, FilteredAppointments, Appointment } from "./App";
import { MemoryRouter } from "react-router";
import Adapter from "enzyme-adapter-react-16";
import { RestContext } from "./getAllAppointments";
import { async, resolve } from "q";

async function getAllAppointments() {
  return [
    { name: "christine", timestamp: new Date("2018").valueOf() },
    { name: "jane", timestamp: new Date("2017").valueOf() },
    { name: "christine", timestamp: new Date("2018").valueOf() },
    { name: "christine", timestamp: new Date("2016").valueOf() }
  ];
}
async function getAppointmentsByName(name: string) {
  return [
    { name: name, timestamp: new Date("2017").valueOf() }
  ];
}

Enzyme.configure({ adapter: new Adapter() });

describe("routes using memory router", () => {
  it("should show AllAppointments component for /appointments route", async () => {
    const myComponent = mount(
      <MemoryRouter initialEntries={["/appointments"]}>
        <RestContext.Provider
          value={{ getAllAppointments, getAppointmentsByName }}
        >
          <App />
        </RestContext.Provider>
      </MemoryRouter>
    );
    await new Promise(resolve => setTimeout(resolve, 1000));
    // optimize date format issue when testing on a different machine asap
    expect(myComponent.html()).toBe("<ul><li>christine, 01/01/2016</li><li>jane, 01/01/2017</li><li>christine, 01/01/2018</li><li>christine, 01/01/2018</li></ul>");
  });

  it("should show AllAppointments component for /appointments route", async () => {
    const myComponent = mount(
      <MemoryRouter initialEntries={["/appointments/filter/jane"]}>
        <RestContext.Provider
          value={{ getAllAppointments, getAppointmentsByName }}
        >
          <App />
        </RestContext.Provider>
      </MemoryRouter>
    );
    await new Promise(resolve => setTimeout(resolve, 1000));
    expect(myComponent.html()).toBe("<ul><li>jane, 01/01/2017</li></ul>");
  });

});
