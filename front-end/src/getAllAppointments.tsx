import React from "react";
import axios from "axios";
import { Appointment } from "./App";

const apiRoot = "/api";

async function getAllAppointments(): Promise<Array<Appointment>> {
  let response = await axios.get(`${apiRoot}/appointments`);
  return response.data;
  // return [
  //   { name: "christine", timestamp: new Date("2018").valueOf() },
  //   { name: "amelie", timestamp: new Date("2017").valueOf() },
  //   { name: "christine", timestamp: new Date("2018").valueOf() },
  //   { name: "christine", timestamp: new Date("2016").valueOf() }
  // ];
}
async function getAppointmentsByName(
  name: string
): Promise<Array<Appointment>> {
  let response = await axios.get(`${apiRoot}/appointments/${name}`);
  return response.data;
  // return [
  //   { name: name + " christine", timestamp: new Date("2018").valueOf() },
  //   { name: name + " amelie", timestamp: new Date("2017").valueOf() },
  //   { name: name + " christine", timestamp: new Date("2018").valueOf() },
  //   { name: name + " christine", timestamp: new Date("2016").valueOf() }
  // ];
}
export const RestContext = React.createContext({
  getAllAppointments,
  getAppointmentsByName
});
