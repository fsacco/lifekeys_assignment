package main

import (
    "fmt"
    "log"
    "net/http"
    "encoding/json"
    "github.com/gorilla/mux"
)

type Appointment struct {
    Name string `json:"name"`
    Timestamp int `json:"timestamp"`
}

// to simulate a database
var appointments []Appointment

func returnAllAppointments(w http.ResponseWriter, r *http.Request){
    fmt.Println("Endpoint Hit: returnAllAppointments")
    json.NewEncoder(w).Encode(appointments)
}

func returnAppointmentsByName(w http.ResponseWriter, r *http.Request){
    vars := mux.Vars(r)
    key := vars["name"]
    matched := []Appointment{}

    for _, a := range appointments {
        if a.Name == key {
            matched = append(matched, a)
        }
    }
    json.NewEncoder(w).Encode(matched)
}

func homePage(w http.ResponseWriter, r *http.Request){
    fmt.Fprintf(w, "Working!")
    fmt.Println("Endpoint Hit: homePage")
}

func handleRequests() {
    // creates a new instance of a mux router
    myRouter := mux.NewRouter().StrictSlash(true)
    // replace http.HandleFunc with myRouter.HandleFunc
    myRouter.HandleFunc("/", homePage)
    myRouter.HandleFunc("/appointments", returnAllAppointments)
    myRouter.HandleFunc("/appointments/{name}", returnAppointmentsByName)
    // finally, instead of passing in nil, we want
    // to pass in our newly created router as the second
    // argument
    log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func main() {
    fmt.Println("Rest API with Mux Routers")
    // requirements file appointments
    appointments = []Appointment{
        Appointment{Name: "christine", Timestamp: 1566432000},
        Appointment{Name: "christine", Timestamp: 1566691200},
        Appointment{Name: "jane", Timestamp: 1566518400},
    }
    handleRequests()
}